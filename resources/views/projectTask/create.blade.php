<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>Item</title>

      <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      
      <style>
          html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
          }

          .full-height {
            height: 100vh;
          }

          .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
          }

          .content {
            text-align: center;
          }
      </style>
    </head>
    <body>
    <div class="flex-center position-ref full-height">
      <div class="container">
        <div>
          <a href="{{ route('projectTask.index')}}" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">go back</a>
        </div>
        <br>
        <h1> Create Tasks </h1>
        @if($errors->any())
                @foreach($errors->all() as $error)
                  <div class="alert alert-danger">
                    {{ $error }}
                  </div>
                @endforeach
            @endif
        <form action="{{route('projectTask.store')}}" method="POST">
            @csrf
            <div class="form-group">
              <label for="name">Task name</label>
              <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
            </div>
            <div class="form-group">
                  <label for="description"> Description</label>
                  <textarea name="description" id="description" class="form-control">{{ old('description') }}</textarea>
            </div>
            <div class="form-group">
                  <label for="project_id"> Project </label>
                  <select name="project_id" class="form-control">
                  @foreach($projects as $project)
                    <option value="{{$project->id}}" {{ (old("category") == 'category1' ? "selected":"") }}> {{ $project->name }} </option>
                  @endforeach
                  </select>
            </div>

            <div class="form-group">
                  <label for="deadline"> Deadline</label>
                  <input type="date" id="start" name="deadline" min="{{ now()->toDateTimeString('Y-m-d') }}" max="2020-12-31">
            </div>

            

            <br>
            <button type="submit" class="btn btn-primary">Create</button>
        </form>    
      </div>  
      </div>
    </body>
</html>