<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                padding-top: 100px;
                display: flex;
                justify-content: center;
            }

            .content {
                text-align: center;
            }


            
        </style>
    </head>
    <body>
        <div class=" position-ref full-height">
            <div class="content">
                <div class="container">
                <h1> Projects </h1>
                <form action="{{ route('project.store') }}" method="POST" class="form-inline">
                @csrf
                    <input class="form-control" placeholder="enter a project" type="text" name="name" >
                    <input class="btn btn-primary" type="submit" value="create">
                    
                </form>
                </div>
                <div class="container">
                <br><br>
                <a class="btn btn-secondary btn-lg active" href="{{ route('projectTask.index') }}" role="button">Tasks</a>
                 @if($errors->any())
                @foreach($errors->all() as $error)
                  <div class="alert alert-danger">
                    {{ $error }}
                  </div>
                @endforeach
            @endif
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <form action="" method="GET" class="form-inline">
                    <input class="form-control" placeholder="project name" type="text" name="search" value="{{ @$search }}">
                    <input class="btn btn-primary" type="submit" value="search">
                </form>
                <br>
                <table id="selectedColumn" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="th-sm">Project Name</th>
                            <th class="th-sm">Status</th>
                            <th class="th-sm">delete</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($projects as $project)
                        <tr>
                            <td>{{ $project->name }}</td>
                            <td><form method="POST" action="{{ route('ProjectStatus.update', $project->id) }}">
                            @csrf
                                <div class="form-group">
                                    <input type="submit" class="btn btn-active" value="{{$project->status}}">
                                </div>
                            </form></td>
                            <td>
                            <form method="POST" action="{{ route('project.destroy', $project->id) }}">
                                @csrf
                                @method('DELETE')
                                <div class="form-group">
                                    <input type="submit" class="btn btn-danger" value="Delete">
                                </div>
                            </form></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{ $projects->links() }}
        </div>
    </body>
</html>