<?php

namespace App\Providers;

use App\Repositories\Project\ProjectRepositoryInterface;
use App\Repositories\Project\ProjectRepository;

use App\Repositories\ProjectTask\ProjectTaskRepositoryInterface;
use App\Repositories\ProjectTask\ProjectTaskRepository;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            ProjectRepositoryInterface::class, 
            ProjectRepository::class
        );

        $this->app->bind(
            ProjectTaskRepositoryInterface::class, 
            ProjectTaskRepository::class
        );
    }
}