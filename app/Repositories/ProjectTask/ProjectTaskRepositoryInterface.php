<?php

namespace App\Repositories\ProjectTask;

interface ProjectTaskRepositoryInterface
{
    public function all();

    public function search($search);

    public function getProjects();

    public function store($request);

    public function statusUpdate($id);

}