<?php

namespace App\Repositories\ProjectTask;

use App\Models\ProjectTask;
use App\Models\Project;
use App\Repositories\ProjectTask\ProjectTaskRepositoryInterface;

class ProjectTaskRepository implements ProjectTaskRepositoryInterface
{
    public function all()
    {
        $projectTasks = ProjectTask::all();

        return $projectTasks;
    }

    // return search value
    public function search($search)
    {
        if (filled($search)) {
            $projects = ProjectTask::where('name', $search)->latest()->paginate(10);
        }
        else {
            $projects = ProjectTask::latest()->paginate(10);
        }

        foreach ($projects as $project){
            $time = now()->format('Y-m-d');
            if($time > $project->deadline && $project->status != 'completed'){
                $project->status  = 'failed';
                $project->save();
            }
        }
        return $projects;
    }

    // gets all projects
    public function getProjects()
    {
        $projects = Project::all();

        return $projects;
    }

    //creates a new task
    public function store($request)
    {
        $projectTask = new ProjectTask;

        $projectTask->project_id = $request->project_id;
        $projectTask->name = $request->name;
        $projectTask->description = $request->description;
        $projectTask->deadline = $request->deadline;

        $projectTask->save();

        return;
    }

    public function statusUpdate($id)
    {
        $task = ProjectTask::where('id', $id)->first();
        
        if($task->status != 'failed' && $task->status == 'pending'){
            $task->status = 'completed';
            $task->save();
        }else{
            $task->status = 'pending';
            $task->save();
        }

        return;
    }

}