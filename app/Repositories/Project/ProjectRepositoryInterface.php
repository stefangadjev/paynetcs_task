<?php

namespace App\Repositories\Project;

interface ProjectRepositoryInterface
{
    public function all();

    public function store($request);

    public function search($search);

    public function statusUpdate($id);
}