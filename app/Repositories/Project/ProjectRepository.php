<?php

namespace App\Repositories\Project;

use App\Models\Project;
use App\Repositories\Project\ProjectRepositoryInterface;

class ProjectRepository implements ProjectRepositoryInterface
{
    public function all()
    {
        return Project::all();
    }

    public function store($request)
    {
        $project = new Project;

        $project->name = $request->name;

        $project->save();

        return;
    }

    public function search($search)
    {
        if (filled($search)) {
            $projects = Project::where('name', $search)->latest()->paginate(10);
        }
        else {
            $projects = Project::latest()->paginate(10);
        }

        return $projects;
    }

    public function statusUpdate($id)
    {
        $task = Project::where('id', $id)->first();
        
        if($task->status == 'pending'){
            $task->status = 'completed';
            $task->save();
        }else{
            $task->status = 'pending';
            $task->save();
        }

        return;
    }

}