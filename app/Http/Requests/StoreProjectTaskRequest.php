<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProjectTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:190', 
            'description' => 'required|string|max:500', 
            'project_id' => 'required|exists:projects,id', 
            'deadline'   => 'required|date|date_format:Y-m-d|after:after:yesterday',
        ];
    }
}
