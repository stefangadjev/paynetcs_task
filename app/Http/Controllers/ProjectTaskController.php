<?php

namespace App\Http\Controllers;

use App\Models\ProjectTask;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\StoreProjectTaskRequest;
use App\Repositories\ProjectTask\ProjectTaskRepositoryInterface;

class ProjectTaskController extends Controller
{
    private $ProjectTask;

    public function __construct(ProjectTaskRepositoryInterface $ProjectTask)
    {
        $this->ProjectTask = $ProjectTask;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $search = Input::get('search');
    
        $projectTasks = $this->ProjectTask->search($search);

        return view('projectTask.index')->with('projectTasks', $projectTasks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = $this->ProjectTask->getProjects();

        return view('projectTask.create',compact('projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProjectTaskRequest $request)
    {
       
        $store = $this->ProjectTask->store($request);

        return redirect()->route('projectTask.index')->with('message', 'Task successfully created');
    }


    public function statusUpdate($id)
    {
        $statusProject = $this->ProjectTask->statusUpdate($id);

        return redirect()->route('projectTask.index');

    }
    

    public function apiShow()
    {
        $tasks = $this->ProjectTask->all();

        return $tasks->toJson();
    }


}
