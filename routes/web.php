<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//project
Route::get('/', 'ProjectController@index')->name('project.index');
Route::post('/createProject', 'ProjectController@store')->name('project.store');
Route::delete('/destroyProject/{project}', 'ProjectController@destroy')->name('project.destroy');
Route::post('/project/statusupdate/{id}', 'ProjectController@statusUpdate')->name('ProjectStatus.update');


//projectTasks
Route::get('/Tasks', 'ProjectTaskController@index')->name('projectTask.index');
Route::get('/Task/create', 'ProjectTaskController@create')->name('projectTask.create');
Route::post('/Task/store', 'ProjectTaskController@store')->name('projectTask.store');
Route::post('/Task/statusupdate/{id}', 'ProjectTaskController@statusUpdate')->name('TaskStatus.update');

